<!Doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- 
Many thanks to 9GAG.com for letting me use this amazing content ! :D
-->

<!-- CSS -->
<link href="app-css/app-main.css" type="text/css" rel="stylesheet" />

<!-- JS -->
<script type="application/javascript" src="app-js/app-jquery.js"></script>
<script type="application/javascript" src="app-js/app-lite.js"></script>


<!-- META -->
<meta name="author" content="Jonathan Levy,JAle,Jalehouse"/> 
<meta name="description" content="9GAG Lite - Still for fun but lighter ! Fun image collection for mobile devices." /> 
<link rel="apple-touch-icon" href="app-img/9lite.png" />

<title>9GAG Lite</title>

<link rel="shortcut icon" href="" type="image/x-icon" />
</head>
<body>
    