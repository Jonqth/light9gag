<?php
  if(defined("AUTOLOAD")) //Definition de l'autoload.
  {
	  function __autoload($nomClass) //initialisation de la function magic __autoload avec un argument
	  {
		  $nomFichier="app-class/".$nomClass.".class.php"; //la variable du nom de fichier prend la valeur nomClass.class.php.
		  if(file_exists($nomFichier)) //vérification de l'existance du fichier.
		  {
			require_once($nomFichier); //demande unique avec le nom du fichier.
		  }
		
	  }
  }
?>