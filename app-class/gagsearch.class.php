<?php
	/**
    * We start building the page with html
    * @head + main + footer
    */
	class gagSearch
	{
		var $main_content;
		
		public function __construct()
		{}
		
		/* ==================================================================================================== */
		/* * VIA TUMBLR ======================================================================================= */
		/**
		* Get 9gag content from tumblr api
		* @JSON
		*/
		public function getGagContent($t_name)
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $t_name);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_HEADER, false);
			return $this->treatGagContent(curl_exec($c));	
		}
		
		/**
		* Render json object converted
		*/
		private function treatGagContent($t_content)
		{return (object)json_decode($t_content);}
		
		/**
		* Display posts
		* @json object converted (params)
		*/
		public function displayGagContent($gt_json)
		{
			foreach($gt_json->response->posts as $posts){
				if(!empty($posts->caption))
				{$g_title = strip_tags($posts->caption);}
				else {$g_title ='';}
				
				$g_url = $posts->photos[0]->alt_sizes[0]->url;
				?>
                	<section>
                        <a title="<?php echo $g_title; ?>" href="<?php echo $g_url; ?>"><img src="<?php echo $g_url; ?>" alt="<?php echo $g_title; ?>" /></a>
                        <p><?php echo $g_title; ?></p>
                    </section>
                <?php
			}
		}
		/**
		* Debug function from tumblr
		* @string
		*/
		public function getDebugg($t_name)
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $t_name);
			curl_setopt($c, CURLOPT_TIMEOUT,20); 
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_HEADER, false);
			$debugg_c = curl_exec($c);
			return var_dump($debugg_c);	
		}
		/* * VIA TUMBLR ======================================================================================= */
		/* ==================================================================================================== */
		
		/* ==================================================================================================== */
		/* * DIRECT 9GAG ====================================================================================== */
		/**
		* Direct get from 9gag.com complete string
		* @string
		*/
		public function getDirect($g_name)
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7');
			curl_setopt($c, CURLOPT_URL, $g_name);
			curl_setopt($c, CURLOPT_TIMEOUT,15); 
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_HEADER, false);
			
			$this->main_content = curl_exec($c);
			return $this->treatDirect(curl_exec($c));	
		}
		
		/**
		* Treatment of 9gag.com complete string
		* @JSON
		*/
		private function treatDirect($g_string)
		{
			//preg_match("#<img src=\"http://d24w6bsrhbeh9d.cloudfront.net/photo/(.*\w*?)\" alt=\"(.*\w*?)\">#",$g_string,$g_matches);
			preg_match_all("#<div class=\"content\">(.*\n)*?</div>#",$g_string,$g_matches);
			unset($g_matches[1]);
			
			$g_matches_l = count($g_matches[0]);
			
				for($i=0; $i<$g_matches_l; $i++){
					/*$g_matches[0][$i] = preg_replace("#.<img src=\"http://d24w6bsrhbeh9d.cloudfront.net/photo/(.*\w*?)\" alt=\"(.*\w*?)\">.#",
						"<section><img src=\"$2\" alt=\"$3\"><p>$3</p></section>",$g_matches[0][$i]);*/
					//$g_matches[0][$i] = preg_replace("#<div class=\"content\" style=\"min-height:280px;\">((.*\n)*?)</div>#",
					//"<section class=\"app-dis\" id=\"".$i."\">".$g_url_matches[0]."</section>\n<section class=\"app-hid\" id=\"".$i."\"></section>",$g_matches[0][$i]);
					
					//GOOD
					$g_matches[0][$i] = preg_replace("#<div class=\"content\">((.*\n)*?)</div>#","<section class=\"app-dis\" id=\"".$i."\">$1</section>",$g_matches[0][$i]);
					$g_matches[0][$i] = preg_replace("#<div class=\"fatbigdick\"></div>#","",$g_matches[0][$i]);
					$g_matches[0][$i] = preg_replace("#<a href=\"/gag/([0-9]*)\">((.*\n)*?)</a>#","$2",$g_matches[0][$i]);
					$g_matches[0][$i] = preg_replace("#alt=\"(.*\w*?)\"/>#","alt=\"$1\"/><p>$1</p>",$g_matches[0][$i]);
				}
				
			return $g_matches;
		}
		
		public function getNextPageNumber($type = '')
		{
			if(empty($type))
			{$type = 'hot';}
			
			preg_match_all("#<a id=\"jump_next\" class='next' href=\"/".$type."/([0-9]+)\">#",$this->main_content, $g_matches);
			return preg_replace("#<a id=\"jump_next\" class='next' href=\"/".$type."/([0-9]+)\">#","$1",$g_matches[0][0]);	
	
		}
		
		public function debug($dbg){
			
			return $dbg;
		}
		/* * DIRECT 9GAG ====================================================================================== */
		/* ==================================================================================================== */
 
	}
?>