<?php
	/**
    * We define the autoload function to get classes dynamically
    * @void
	* -> require_once class
    */
	define("AUTOLOAD",true);
	define("HTML",true); 
	require_once("app-inc/autoload.inc.php");
	require("app-inc/const.inc.php");
	
	/**
    * Instanciate a new gagSearch object
    **/
	$gagSearch = new gagsearch();
	
	/**
    * We start building the page with html
    * @head + main + footer
    */
	include("app-inc/head.inc.php");
	include("app-inc/main.inc.php");
	include("app-inc/footer.inc.php");
?> 